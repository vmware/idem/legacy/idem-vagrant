import pytest


def test_present(hub, ctx, instance_name):
    hub.states.vagrant.present(ctx, instance_name)


def test_absent(hub, ctx, instance_name):
    hub.states.vagrant.absent(ctx, instance_name)
